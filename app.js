/**
 * app.js
 *
 * Use `app.js` to run your app without `sails lift`.
 * To start the server, run: `node app.js`.
 *
 * This is handy in situations where the sails CLI is not relevant or useful,
 * such as when you deploy to a server, or a PaaS like Heroku.
 *
 * For example:
 *   => `node app.js`
 *   => `npm start`
 *   => `forever start app.js`
 *   => `node debug app.js`
 *
 * The same command-line arguments and env vars are supported, e.g.:
 * `NODE_ENV=production node app.js --port=80 --verbose`
 *
 * For more information see:
 *   https://sailsjs.com/anatomy/app.js
 */


// Ensure we're in the project directory, so cwd-relative paths work as expected
// no matter where we actually lift from.
// > Note: This is not required in order to lift, but it is a convenient default.
const nodemailer = require('nodemailer');

process.chdir(__dirname);



// Attempt to import `sails` dependency, as well as `rc` (for loading `.sailsrc` files).
var sails;
var rc;
try {
  sails = require('sails');
  rc = require('sails/accessible/rc');
} catch (err) {
  console.error('Encountered an error when attempting to require(\'sails\'):');
  console.error(err.stack);
  console.error('--');
  console.error('To run an app using `node app.js`, you need to have Sails installed');
  console.error('locally (`./node_modules/sails`).  To do that, just make sure you\'re');
  console.error('in the same directory as your app and run `npm install`.');
  console.error();
  console.error('If Sails is installed globally (i.e. `npm install -g sails`) you can');
  console.error('also run this app with `sails lift`.  Running with `sails lift` will');
  console.error('not run this file (`app.js`), but it will do exactly the same thing.');
  console.error('(It even uses your app directory\'s local Sails install, if possible.)');
  return;
}//-•


// Start server
sails.lift(rc('sails'));
console.log('Good morning!')
sendEmail().then((info)=>{
  console.log(info)
}).catch((err)=>{
  console.log(err);
});


function sendEmail( ){
  return new Promise((resolve, reject)=>{
      // create reusable transporter object using the default SMTP transport
      console.log(`Sending email `);
      let transporter = nodemailer.createTransport({
        host: '81.29.101.5',
        // host:"Gmail",
        port: '25',
        logger: true,
        debug: true,
        secure: false, // true for 465, false for other ports
        ignoreTLS: true,
        auth: { user: 'ebc.wallet@egyptianbanks.net', pass: 'P@ssw0rd' }
        //  auth: { user: 'contact@paymeapp.co', pass: 'payme12345' },
      });

      // setup email data with unicode symbols
      let mailOptions = {
          from: '"Test Email" <ebc.wallet@egyptianbanks.net>', // sender address
          to: 'abd.allah.zidan@gmail.com', // list of receivers
          subject: 'Test Email', // Subject line
          html: '<p> This is a test email. </p>' // html body
      };
    //   let transporter = nodemailer.createTransport({
    //     service:'Gmail',
    //     port: 587,
    //     secure: false, // true for 465, false for other ports
    //     auth: { user: 'contact@paymeapp.co', pass: 'payme12345' },
    //     logger: true,
    //     tls: {rejectUnauthorized: false},
    //     debug: true
    // });

    // // setup email data with unicode symbols
    // let mailOptions = {
    //     from: '"Test Email" <contact@paymeapp.co>', // sender address
    //     to: 'abd.allah.zidan@gmail.com', // list of receivers
    //     subject: 'Test Email', // Subject line
    //     html: '<p> This is a test email. </p>' // html body
    // };

      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
              console.log(error);
              reject(error);
              // response.send({code:400, message:"Error sending the email"});

          }
          resolve(info);
          // response.send({code:200, message:"Ok"});
          // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
          // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      });
  })
  
}
