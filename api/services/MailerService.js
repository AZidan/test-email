function sendEmail( templateData ){
    return new Promise((resolve, reject)=>{
        // create reusable transporter object using the default SMTP transport
        console.log(`Sending email with data ${templateData}`)
        let transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'info@kam-egypt.com', // generated ethereal user
                pass: '123456987' // generated ethereal password
            }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"KAM Team" <info@kam-egypt.com>', // sender address
            to: templateData.to, // list of receivers
            cc: templateData.cc,
            bcc: templateData.bcc,
            subject: templateData.subject, // Subject line
            html: templateData.body // html body
        };
        var smsText = templateData.body.replace(/<\/?[^>]+(>|$)/g, "");
        var smsAPI = "https://www.smsmisr.com/api/send/?username=z1TBUgoG&password=njJjLYAgpQ&language=2&sender=KAM&mobile="+templateData.phone+"&message="+smsText
        
        templateData.phone && https.get(encodeURI(smsAPI), res=> console.log("SMS Sent", smsAPI));
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                reject(error);
                // response.send({code:400, message:"Error sending the email"});

            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            resolve(info);
            // response.send({code:200, message:"Ok"});
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    })
    
}
